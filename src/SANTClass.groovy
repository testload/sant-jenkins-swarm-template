@GrabResolver(name='sonatype-snapshots',    root='https://oss.sonatype.org/content/groups/public/') //debuglibs

@Grapes([
        @Grab(group='cloud.testload', module='sant-library', version='1.9.69')
])
import cloud.testload.SANTLib

class SANTClass extends SANTLib {
}