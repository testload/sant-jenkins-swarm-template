def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Ожидаем выполнения нагрузки"

    new SANTClass(script:this).jenkinsWaitLoading(config.processProfileJSON);
    echo "Выполнено"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return this;
}