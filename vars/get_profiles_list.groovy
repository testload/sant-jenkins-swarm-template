def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список профилей"
    def list=new SANTClass(script:this,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost).confluenceGetProfilesList()

    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def profilesList=''
    list.each{
        profilesList=profilesList+it.toString()+'\n'
    }

    return profilesList;
}