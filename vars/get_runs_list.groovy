def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список результатов для "+env.ProfileName

    def list=new SANTClass(script:this,
            dbName:"jmresults_statistic",
            clickHouseUrl:config.clickHouseUrl,
            clickHouseLogin:config.clickHouseLogin,
            clickHousePassword:config.clickHousePassword).clickHouseGetRunsListByProfileName(env.ProfileName)
    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def runsList=''
    list.each{
        runsList=runsList+it.toString()+'\n'
    }

    return runsList;
}