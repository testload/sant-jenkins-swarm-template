def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Создаем набор микросервисов"
    new SANTClass(script:this,
            clickHouseUrl:config.clickHouseUrl,
            clickHouseLogin:config.clickHouseLogin,
            clickHousePassword:config.clickHousePassword,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            lokiUrl:config.lokiUrl,
            lokiAuth:config.lokiAuth,
            userName:env.UserName,
            lazyListener:true,
            gitToken:config.gitToken).dockerCreateServices(env.ProfileName,config.runIdForServices,config.processProfileJSON);
    echo "Выполнено успешно"

    echo "Ожидаем пока поднимутся, 1 минуту."
    new SANTClass(script:this).jenkinsWaitBuffer();
    echo "Выполнено успешно"


    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return this;
}